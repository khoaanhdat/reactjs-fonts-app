const NewsAPI = require('newsapi');
const newsapi = new NewsAPI('eb4607fe1a7a48be9c969950afe4d0d2', { corsProxyUrl: 'https://cors-anywhere.herokuapp.com/' });

export const GetListNews = async () => {
  const response = newsapi.v2.everything({
    q: 'Apple',
    from: '2021-12-11',
    sortBy: 'popularity'
  });

  const data = await response;
  const listNews = data.articles;

  if (data.status !== 'ok') {
      throw new Error(data.message || "Cannot get data. Please try again.");
    }

  return [...listNews];
};