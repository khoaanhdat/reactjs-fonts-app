import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";

i18n.use(LanguageDetector).init({
    lng: "en",
    resources: {
        en: {
            translations: {
                "Search": "Search",
                "News": "News",
            }
        },
        vi: {
            translations: {
                "Search": "Tìm Kiếm",
                "News": "Tin Tức",
            }
        },
        cn: {
            translations: {
                "Search": "搜索",
                "News": "消息",
            }
        },
        th: {
            translations: {
                "Search": "ค้นหา",
                "News": "ข่าว"
            }
        },
    },
    fallbackLng: 'en',
    debug: false,
    ns: ["translations"],
    defaultNS: "translations",

    keySeparator: false,

    interpolation: {
        escapeValue: false,
        formatSeparator: ","
    },

    react: {
        wait: true
    }
});

export default i18n;
