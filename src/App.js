import { Routes, Route, useNavigate } from 'react-router-dom';
import { useLocation } from "react-router";

import './App.css';
import Layout from './components/Layout/Layout';
import Home from './pages/Home';
import News from './pages/News';
import NotFound from './pages/NotFound';
import { useContext, useEffect } from 'react';
import TranslateContext from './store/translate-context';

function App() {
  const { currentLanguage, changedLanguage } = useContext(TranslateContext);
  const navigate = useNavigate();

  const { pathname } = useLocation();

  useEffect(() => {
    const lng = pathname.substr(1, 2);

    if (lng === '') {
      if (currentLanguage !== 'en') {
        navigate(currentLanguage);
      }
    } else {
      if (lng !== currentLanguage) {
        changedLanguage(lng);
        navigate(pathname);
      }
    }
  }, [currentLanguage, changedLanguage, navigate, pathname]);

  return (
    <Layout>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/:locale" element={<Home />} />
        <Route path="/:locale/*">

          <Route exact path='news' element={<News />} />

          <Route path='*' element={<NotFound />} />
        </Route>


      </Routes>
    </Layout>
  );
}

export default App;
