import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import './index.css';
import App from './App';
import SwitcherProvider from './store/SwitcherProvider';
import TranslateProvider from './store/TranslateProvider';
import AuthProvider from './store/AuthProvider';
import { MessageContextProvider } from './store/message-context';

ReactDOM.render(
  <MessageContextProvider>
    <AuthProvider>
      <TranslateProvider>
        <SwitcherProvider>
          <BrowserRouter>
            <App />
          </BrowserRouter>
        </SwitcherProvider>
      </TranslateProvider>
    </AuthProvider>
  </MessageContextProvider>,
  document.getElementById('root')
);
