import React from 'react';

const SwitcherContext = React.createContext({
    themeColor: '',
    isShowThemeOptions: false,
    toggleThemeOptions: () => {},
    selectTheme: (theme) => { }
});

export default SwitcherContext;