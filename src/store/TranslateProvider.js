import React, { useState } from 'react';

import TranslateContext from './translate-context';
import i18n from '../translate/i18n';

const TranslateProvider = props => {
    const [currentLanguage, setCurrentLanguage] = useState(i18n.language);
    const [isShowLngOptions, setIsShowLngOptions] = useState(false);

    const changedLanguageHandler = (key) => {
        i18n.changeLanguage(key);
        setCurrentLanguage(key);
    }

    const translateText = (text) => {
        return i18n.t(text);
    }

    const toggleShowLngOptions = () => {
        setIsShowLngOptions(!isShowLngOptions);
    }

    const translateContext = {
        isShowLngOptions,
        toggleShowLngOptions,
        currentLanguage,
        translate: translateText,
        changedLanguage: changedLanguageHandler
    };

    return (
        <TranslateContext.Provider value={translateContext}>
            {props.children}
        </TranslateContext.Provider>
    );
}

export default TranslateProvider;