import React, { useEffect, useState } from 'react';

import AuthContext from './auth-context';

const AuthProvider = props => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [isLoginFormShown, setIsLoginFormShow] = useState(false);

    useEffect(() => {
        const storedUserLoggedInInformation = localStorage.getItem('username');

        if (storedUserLoggedInInformation) {
            setIsLoggedIn(true)
        }
    }, []);

    const toggleFormLogin = () => {
        setIsLoginFormShow(!isLoginFormShown);
    }

    const onLogout = () => {
        localStorage.removeItem('username');
        setIsLoggedIn(false);
    }

    const onLogin = (name) => {
        localStorage.setItem('username', name);
        setIsLoggedIn(true);
    }

    const getUserName = () => {
        return localStorage.getItem('username');
    }

    const authContext = {
        isLoggedIn,
        isLoginFormShown,
        toggleFormLogin,
        onLogout,
        onLogin,
        getUserName
    };

    return (
        <AuthContext.Provider value={authContext}>
            {props.children}
        </AuthContext.Provider>
    );
}

export default AuthProvider;