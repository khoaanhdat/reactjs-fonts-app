import React from 'react';

const AuthContext = React.createContext({
    isLoggedIn: false,
    isLoginFormShown: false,    
    toggleFormLogin: () => {},
    onLogout: () => { },
    onLogin: (name) => { },
    getUserName: () => {}
});

export default AuthContext;