import React, { useState } from 'react';

import SwitcherContext from './switcher-context';

const SwitcherProvider = props => {
    let themeDefault = localStorage.getItem('theme-color');
    
    const [themeColor, setThemeColor] = useState(themeDefault || 'dark');
    const [isShowThemeOptions, setIsShowThemeOptions] = useState(false);

    const selectThemeHandler = (theme) => {
        setThemeColor(theme);
         localStorage.setItem('theme-color', theme);
    }

    const toggleThemeOptions = () => {
        setIsShowThemeOptions(!isShowThemeOptions);
    }

    const switcherContext = {
        themeColor,
        isShowThemeOptions,
        toggleThemeOptions,
        selectTheme: selectThemeHandler
    };

    return (
        <SwitcherContext.Provider value={switcherContext}>
            {props.children}
        </SwitcherContext.Provider>
    );
}

export default SwitcherProvider;