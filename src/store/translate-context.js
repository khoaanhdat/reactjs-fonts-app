import React from 'react';

const TranslateContext = React.createContext({
    isShowLngOptions: false,
    toggleShowLngOptions: () => {},
    currentLanguage: 'en',
    translate: (text) => {},
    changedLanguage: (key) => { }
});

export default TranslateContext;