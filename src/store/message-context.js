import React, { useCallback, useEffect, useState } from "react";

export const MessageContext = React.createContext({
    isShow: false,
    messages: [],
    addMessage: (text, className) => { }
});

export const MessageContextProvider = (props) => {
    const [isShow, setIsShow] = useState(false);
    const [messages, setMessages] = useState([]);

    const addMessage = useCallback((text, className) => {
        setMessages([...messages, { 'text': text, 'className': className }]);
        setIsShow(true);
    }, [messages]);

    useEffect(() => {
        let timeLife = setTimeout(() => {
            let listMessage = [...messages];
            listMessage.pop();
            setMessages(listMessage);
            if (!messages.length) {
                setIsShow(false);
                clearTimeout(timeLife);
            }
        }, 5000);
        return () => {
            clearTimeout(timeLife);
        };

    }, [messages]);

    const contextValue = {
        isShow,
        messages,
        addMessage
    }

    return <MessageContext.Provider value={contextValue}>{props.children}</MessageContext.Provider>
}