import React, { useEffect, useState } from "react";

import classes from './Datetime.module.scss';

const DateTime = () => {
    const [time, setTime] = useState(getTime());

    function getTime() {
        let date = new Date();

        let time = {
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            days: date.getDate(),
            hours: date.getHours(),
            minutes: date.getMinutes(),
            seconds: date.getSeconds()
        };

        return time;
    }

    useEffect(() => {
        const timer = setTimeout(() => {
            setTime(getTime());
        }, 1000);

        return () => {
            clearTimeout(timer);
        };
    }, [time]);

    return (
        <div className={classes.datetime}>
            <span>{time.days}/{time.month}/{time.year}, {time.hours}:{time.minutes}:{time.seconds}</span>
        </div>
    )
}

export default DateTime