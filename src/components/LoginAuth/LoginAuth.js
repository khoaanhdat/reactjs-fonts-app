import { useContext, useRef } from "react";
import AuthContext from "../../store/auth-context";

import Modal from "../UI/Modal";
import classes from './LoginAuth.module.scss';

const LoginAuth = () => {
    const { toggleFormLogin, onLogin } = useContext(AuthContext);

    const nameRef = useRef();

    const hideLoginForm = () => {
        toggleFormLogin()
    }

    const loginHandler = (e) => {
        e.preventDefault();

        const name = nameRef.current.value;
        if (name) {
            onLogin(name);
            toggleFormLogin();
        }
    }

    return (
        <Modal onClose={hideLoginForm} title="Login">
            <form className={classes['form-group']}>
                <input id="username" ref={nameRef} type="text" placeholder="Enter your name"/>
                <button onClick={(e) => loginHandler(e)}>Login</button>
            </form>
        </Modal>
    );
}

export default LoginAuth;