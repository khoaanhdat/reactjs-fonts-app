import { GetListNews } from "../../lib/api";
import useHttp from "../../hooks/use-http";

import classes from './NewList.module.scss';
import { useContext, useEffect } from "react";
import TranslateContext from "../../store/translate-context";

const NewList = () => {
    const {translate} = useContext(TranslateContext);

    const {
        sendRequest,
        status,
        data: listNews,
        error,
    } = useHttp(GetListNews, true);

    useEffect(() => {
        sendRequest();
    }, [sendRequest]);

    if (status === "pending") {
        return <span>Loading...</span>;
    }

    if (error) {
        return <span>{error}</span>;
    }

    if (status === "completed" &&
        (!listNews || listNews.length === 0)
    ) {
        return <span>Nothing Data</span>;
    }

    return (
        <div className={classes.news}>
            <div className={classes['news-title']}><h1>{translate('News')}</h1></div>
            {listNews.map((item, key) => {
                return (
                    <div key={key} className={classes['news-item']}>
                        <div className={classes['news-thumbnail']}>
                            <img src={item.urlToImage} alt={item.title} />
                        </div>
                        <div className={classes['news-content']}>
                            <h2 className={classes['news-title']}>{item.title}</h2>
                            <p className={classes['news-author']}>By <span>{item.author}</span></p>
                            <p className={classes['news-des']}>{item.description}</p>
                            <a rel="noreferrer" target="_blank" href={item.url}>Read more</a>
                        </div>
                    </div>
                );
            })}

        </div>
    );
}

export default NewList;