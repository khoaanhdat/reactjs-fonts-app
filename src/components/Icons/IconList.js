import { useContext, useState } from 'react';
import { MessageContext } from '../../store/message-context';
import SwitcherContext from '../../store/switcher-context';
import TranslateContext from '../../store/translate-context';

import classes from './IconList.module.scss';

const iconsClass = [
    "ai-active-rain",
    "ai-arrow-a-n",
    "ai-arrow-a-p",
    "ai-arrow-b-n",
    "ai-arrow-b-p",
    "ai-arrow-c-n",
    "ai-arrow-c-p",
    "ai-arrow-d-n",
    "ai-arrow-d-p",
    "ai-arrow-e-n",
    "ai-arrow-e-p",
    "ai-arrow-i-d",
    "ai-arrow-i-n",
    "ai-arrow-i-p",
    "ai-arrow-i-u",
    "ai-bbb",
    "ai-bed",
    "ai-bed-a",
    "ai-blogger",
    "ai-buildings",
    "ai-caimeiju",
    "ai-calendar",
    "ai-cameo",
    "ai-cameo-longform",
    "ai-car",
    "ai-cellphone",
    "ai-check-a",
    "ai-check-list",
    "ai-check-o",
    "ai-close-a",
    "ai-close-b",
    "ai-close-c",
    "ai-close-d",
    "ai-close-e",
    "ai-close-f",
    "ai-close-g",
    "ai-coin",
    "ai-comment-a",
    "ai-credit-card",
    "ai-credit-card-alt",
    "ai-document-a",
    "ai-dollar-o",
    "ai-eho",
    "ai-envelope",
    "ai-envelope-f",
    "ai-envelope-o",
    "ai-facebook",
    "ai-fax",
    "ai-file-code",
    "ai-file-excel",
    "ai-file-f",
    "ai-file-image-o"
];

const IconList = () => {
    const [iconList, setIconList] = useState(iconsClass);

    const { translate } = useContext(TranslateContext);

    const { addMessage } = useContext(MessageContext);

    const { themeColor } = useContext(SwitcherContext);

    const searchHandler = (e) => {
        const txtSearch = e.target.value.toLowerCase();
        const result = iconsClass.filter(item => item.includes(txtSearch));
        setIconList(result);
    }

    const copyHtmlTag = (code) => {
        const htmlTag = `<i class="${code}"></i> copied!`;
        navigator.clipboard.writeText(htmlTag);
        addMessage(htmlTag, 'success');
    }

    return (
        <div className={classes['icons']}>
            <h1>Font Icons</h1>
            <div className={classes['icons-search']}>
                <input type="text" placeholder={translate('Search')} onChange={(e) => searchHandler(e)} />
                <i className="ai-magnifying-glass-a"></i>
            </div>
            <div className={classes['icons-list']}>
                {iconList.map((item, key) => {
                    return (
                        <div onClick={() => copyHtmlTag(item)} key={key} className={`${classes['icons-item-wrap']} ${classes[themeColor]}`}>
                            <div className={classes['icons-icon']}>
                                <i className={item}></i>
                            </div>
                            <div className={classes['icons-code']}>
                                <code>{`<i class="${item}"></i>`}</code>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default IconList;