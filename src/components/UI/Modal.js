import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';

import classes from './Modal.module.scss';

const Backdrop = props => {
    return <div
        className={`${classes.backdrop} ${props.customBackdropClass ? classes[props.customBackdropClass] : ''}`}
    />
}

const ModalOverlay = props => {
    return <div className={`${classes.modal} ${props.customModalClass ? classes[props.customModalClass] : ''}`}>
        {props.title && (
            <div className={classes.title}>
                <span>{props.title}</span>
            </div>
        )}
        <span onClick={props.onClose} className={classes.close}>X</span>

        {props.children}
    </div>
}

const portalElement = document.getElementById('overlays');

const Modal = props => {
    return <Fragment>
        {ReactDOM.createPortal(<Backdrop customBackdropClass={props.customBackdropClass} />, portalElement)}
        {ReactDOM.createPortal(<ModalOverlay customModalClass={props.customModalClass} onClose={props.onClose} title={props.title}>{props.children}</ModalOverlay>, portalElement)}
    </Fragment>
}

export default Modal;