import React from 'react';

import classes from './MessageBox.module.scss';

const MessageBox = ({ messages, isShow }) => {
    return (
        <div className={`${classes.messages} ${isShow ? classes.show : ''}`}>
            {
                (isShow && messages.length > 0) && messages.map((message, key) => {
                    return (
                        <div key={key} className={`${classes['messages-item']} ${classes[message.className]}`}>
                            <div className={classes['body']}>
                                <span>{message.text}</span>
                            </div>
                            <div className={classes['timelife']}></div>
                        </div>
                    )
                })
            }
            
        </div>
    );
}

export default MessageBox;
