import classes from './Footer.module.scss';

const Footer = () => {
    return (
        <footer className={classes.footer}>
            <a href="https://resources.agentimage.com/fonts/agentimage.font.icons.css?ver=5.3.4">Font Source https://resources.agentimage.com/fonts/agentimage.font.icons.css?ver=5.3.4</a>
        </footer>
    );
}

export default Footer;