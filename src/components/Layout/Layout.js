import React, { useContext } from 'react';
import AuthContext from '../../store/auth-context';
import { MessageContext } from '../../store/message-context';
import SwitcherContext from '../../store/switcher-context';
import LoginAuth from '../LoginAuth/LoginAuth';
import MessageBox from '../UI/MessageBox';
import Footer from './Footer/Footer';

import classes from './Layout.module.scss';
import MainHeader from './MainHeader/MainHeader';

const Layout = (props) => {
    const { themeColor } = useContext(SwitcherContext);
    const { isLoginFormShown } = useContext(AuthContext);

    const { isShow, messages } = useContext(MessageContext);

    return (
        <main className={themeColor}>
            <MessageBox isShow={isShow} messages={messages} />
            {isLoginFormShown && <LoginAuth />}
            <MainHeader />
            <div className={`${classes['page-wrapper']}`}>
                {props.children}
            </div>
            <Footer />
        </main>
    );
};

export default Layout;