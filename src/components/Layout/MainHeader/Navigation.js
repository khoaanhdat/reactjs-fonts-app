import { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import TranslateContext from '../../../store/translate-context';

import classes from './Navigation.module.scss';

const Navigation = ({ isShowSidebar, hideSideBar }) => {
    const { currentLanguage } = useContext(TranslateContext);

    return (
        <nav className={`${classes.navs} ${isShowSidebar ? classes['show-mobile'] : ''}`}>
            <span onClick={hideSideBar} className={classes['close']}>x</span>
            <div className={classes['navs-items']}>
                <ul>
                    <li>
                        <NavLink end to={`${currentLanguage === 'en' ? '' : currentLanguage}`} className={({ isActive }) => (isActive ? classes.active : '')}>
                            Home
                        </NavLink>
                        <NavLink to={`${currentLanguage}/news`} className={({ isActive }) => (isActive ? classes.active : '')}>
                            News
                        </NavLink>
                    </li>
                </ul>
            </div>
        </nav>
    );
}

export default Navigation;