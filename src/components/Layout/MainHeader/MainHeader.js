import React, { useContext, useState } from "react";

import Navigation from "./Navigation";
import classes from "./MainHeader.module.scss";
import DateTime from "../../Datetime/Datetime";
import ThemeSwitcher from "../../Switcher/ThemeSwitcher/ThemeSwitcher";
import Language from "../../Switcher/Language/Language";
import AuthContext from "../../../store/auth-context";

const MainHeader = () => {
    const { isLoggedIn, toggleFormLogin, onLogout, getUserName } = useContext(AuthContext);
    const [isShowSidebar, setIsShowSidebar] = useState(false);

    const toggleSidebar = () => {
        setIsShowSidebar(!isShowSidebar);
    }

    return (
        <header className={`${classes.toolbar}`}>
            <div onClick={toggleSidebar} className={classes['toogle-menu']}><i className="ai-menu"></i></div>
            <Navigation hideSideBar={toggleSidebar} isShowSidebar={isShowSidebar}/>
            <DateTime />
            {getUserName() && <span className={classes.username}>Hello, {getUserName()}</span>}
            <ThemeSwitcher />
            <Language />
            <div className={classes.auth}>
                {!isLoggedIn && <button onClick={() => toggleFormLogin()}>Log in</button>}
                {isLoggedIn && <button onClick={() => onLogout()}>Logout</button>}
            </div>
        </header>
    )
}

export default MainHeader;