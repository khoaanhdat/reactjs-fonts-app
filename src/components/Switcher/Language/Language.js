import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { useLocation } from "react-router";

import TranslateContext from "../../../store/translate-context";
import classes from './Language.module.scss';

import viFlag from '../../../assets/images/flag/vi.png';
import enFlag from '../../../assets/images/flag/en.png';
import thFlag from '../../../assets/images/flag/th.png';
import cnFlag from '../../../assets/images/flag/cn.png';

const locale = {
    'en': {
        name: 'English',
        img: enFlag
    },
    'vi': {
        name: 'Việt Nam',
        img: viFlag
    },
    'th': {
        name: 'Thailand',
        img: thFlag
    },
    'cn': {
        name: 'China',
        img: cnFlag
    }
};

const Language = () => {

    const { changedLanguage, currentLanguage, toggleShowLngOptions, isShowLngOptions } = useContext(TranslateContext);

    const { pathname } = useLocation();

    const navigate = useNavigate()

    const changeLanguage = (language) => {
        changedLanguage(language);
        navigate(setUrl(language));
        toggleShowLngOptions();
    }

    const setUrl = (keyLanguage) => {
        const routeComponent = pathname.substring(3);

        if (keyLanguage === 'en' && routeComponent === '') {
            return '/' + routeComponent;
        }
        return '/' + keyLanguage + routeComponent;
    }

    return (
        <div className={classes.language}>
            <span onClick={toggleShowLngOptions} className={`${classes['language-active']} ${classes.flag}`} style={{ 'backgroundImage': 'url(' + locale[currentLanguage]['img'] + ')' }}>{currentLanguage}</span>
            <ul className={`${classes['language-options']} ${isShowLngOptions ? classes['show'] : ''}`}>
                {
                    Object.keys(locale).map(key => {
                        if (key !== currentLanguage) {
                            return (
                                <li key={key} onClick={() => changeLanguage(key)}>
                                    <span>{locale[key].name}</span>
                                    <span className={classes.flag} style={{ 'backgroundImage': 'url(' + locale[key]['img'] + ')' }}></span>
                                </li>
                            );
                        } else {
                            return '';
                        }

                    })
                }
            </ul>
        </div>
    )
}

export default Language;