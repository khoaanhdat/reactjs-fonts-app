import { useContext } from 'react';


import SwitcherContext from '../../../store/switcher-context';
import classes from './ThemeSwitcher.module.scss';

const themeOptions = [
    'light',
    'dark'
];

const ThemeSwitcher = () => {
    let { themeColor, selectTheme, isShowThemeOptions, toggleThemeOptions } = useContext(SwitcherContext);

    if (!themeColor || !themeOptions.includes(themeColor)) {
        themeColor = themeOptions[0];
    }

    const selectThemeHandler = (color) => {
        selectTheme(color);
        toggleThemeOptions();
    }

    return (
        <div className={classes['theme-switcher']}>
            <span onClick={toggleThemeOptions} className={`${classes['theme-show']} ${classes[themeColor]}`}></span>
            <ul className={`${classes['theme-options']} ${isShowThemeOptions ? classes.show : ''}`}>
                {
                    themeOptions.map((item, key) => {
                        return <li
                            onClick={() => selectThemeHandler(item)}
                            key={key}
                        >
                            <span className={`${classes['theme-item']} ${classes[item]}`}></span>
                        </li>;
                    })
                }
            </ul>
        </div>
    )
};

export default ThemeSwitcher;