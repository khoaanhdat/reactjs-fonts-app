import React, { Fragment } from "react";
import IconList from "../components/Icons/IconList";

const Home = (props) => {  
  return (
    <Fragment>
      <IconList />
    </Fragment>
  );
};

export default Home;
